package com.example.Outpatient.Appointment.Application.Controller;

import com.example.Outpatient.Appointment.Application.Model.Appointment_Booking;
import com.example.Outpatient.Appointment.Application.Service.Appointment_Booking_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

@RestController
@Repository
@RequestMapping("/appointment")
public class Appointment_Booking_Controller {
    @Autowired
    Appointment_Booking_Service appointmentBookingServiceObj;
    @PostMapping("/insert")
    public Appointment_Booking addAppointment_Booking(@RequestBody Appointment_Booking appointmentBooking){
        Appointment_Booking obj = appointmentBookingServiceObj.addAppointment_Booking(appointmentBooking);
        return obj;
    }
}
