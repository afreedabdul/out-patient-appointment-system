package com.example.Outpatient.Appointment.Application.Repository;

import com.example.Outpatient.Appointment.Application.Model.Doctor_Details;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Doctor_DetailsRepository extends JpaRepository<Doctor_Details,Integer> {
    Optional<Doctor_Details> findById(int doc_id);

}
