package com.example.Outpatient.Appointment.Application.Repository;

import com.example.Outpatient.Appointment.Application.Model.Doctor_Details;
import com.example.Outpatient.Appointment.Application.Model.Doctor_Listing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Doctor_ListingRepository extends JpaRepository<Doctor_Listing,Integer> {

    Doctor_Details findById(int doc_id);
}
