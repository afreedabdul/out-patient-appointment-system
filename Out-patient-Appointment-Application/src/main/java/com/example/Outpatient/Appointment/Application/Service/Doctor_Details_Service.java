package com.example.Outpatient.Appointment.Application.Service;

import com.example.Outpatient.Appointment.Application.Model.Doctor_Details;
import com.example.Outpatient.Appointment.Application.Repository.Doctor_DetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Doctor_Details_Service {
    @Autowired
    private Doctor_DetailsRepository doctorDetailsRepository;
    public Doctor_Details addDoctor_Details(Doctor_Details doctorDetails) {
        //System.out.print(doctorDetails);
        //try {
            doctorDetailsRepository.save(doctorDetails);
            return doctorDetails;
       // }catch(Exception e){

        //}
        //return " details are added in doctor details page";

    }
    public Doctor_Details findById(int doc_id) {
        return doctorDetailsRepository.findById(doc_id).orElse(null);
    }
}
