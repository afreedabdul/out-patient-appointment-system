package com.example.Outpatient.Appointment.Application.Model;

import jakarta.persistence.*;

@Entity
@Table
public class Appointment_Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "doc_id")
    private Doctor_Details doctorDetails;
    private String patientName;
    private String Day;
    private String Status;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Doctor_Details getDoctorDetails() {
        return doctorDetails;
    }

    public void setDoctorDetails(Doctor_Details doctorDetails) {
        this.doctorDetails = doctorDetails;
    }
}
