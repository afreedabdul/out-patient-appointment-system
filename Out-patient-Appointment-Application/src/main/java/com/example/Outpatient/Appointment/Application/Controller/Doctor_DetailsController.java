package com.example.Outpatient.Appointment.Application.Controller;

import com.example.Outpatient.Appointment.Application.Model.Doctor_Details;
import com.example.Outpatient.Appointment.Application.Repository.Doctor_DetailsRepository;
import com.example.Outpatient.Appointment.Application.Repository.Doctor_ListingRepository;
import com.example.Outpatient.Appointment.Application.Service.Doctor_Details_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Repository
@RequestMapping("/Doctor_Details")
public class Doctor_DetailsController {
    @Autowired
    Doctor_Details_Service doctorDetailsServiceObj;
    @Autowired
    private Doctor_ListingRepository doctor_ListingRepository;
    @Autowired
    private Doctor_DetailsRepository doctor_DetailsRepository;

    @PostMapping("/inserting")
    public Doctor_Details addDoctor_Details(@RequestBody Doctor_Details doctorDetails){
        //System.out.print(doctorDetails);
        Doctor_Details obj = doctorDetailsServiceObj.addDoctor_Details(doctorDetails);
        return obj;
    }
    @GetMapping("/findById/{doc_id}")
    public ResponseEntity<Doctor_Details> getDoctor_DetailsById(@PathVariable int doc_id){
        Doctor_Details doctorDetails = doctorDetailsServiceObj.findById(doc_id);
        if(doctorDetails != null){
            return ResponseEntity.ok(doctorDetails);
        }else {
            return ResponseEntity.notFound().build();
        }
    }
}
