package com.example.Outpatient.Appointment.Application.Repository;

import com.example.Outpatient.Appointment.Application.Model.Appointment_Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Appointment_BookingRepository extends JpaRepository<Appointment_Booking,Integer> {
    Optional<Appointment_Booking> findById(int id);
}
