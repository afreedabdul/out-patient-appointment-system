package com.example.Outpatient.Appointment.Application.Model;

import jakarta.persistence.*;

@Entity
@Table
public class Doctor_Details {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int doc_id;
    private String Full_Name;
    private String Specialization;
    private String Availability;
    private int contactNo;
    private String max_patients;
    private String LeaveOn;

    public int getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(int doc_id) {
        this.doc_id = doc_id;
    }

    public String getFull_Name() {
        return Full_Name;
    }

    public void setFull_Name(String full_Name) {
        Full_Name = full_Name;
    }

    public String getSpecialization() {
        return Specialization;
    }

    public void setSpecialization(String specialization) {
        Specialization = specialization;
    }

    public String getAvailability() {
        return Availability;
    }

    public void setAvailability(String availability) {
        Availability = availability;
    }

    public int getContactNo() {
        return contactNo;
    }

    public void setContactNo(int contactNo) {
        this.contactNo = contactNo;
    }

    public String getMax_patients() {
        return max_patients;
    }

    public void setMax_patients(String max_patients) {
        this.max_patients = max_patients;
    }

    public String getLeaveOn() {
        return LeaveOn;
    }

    public void setLeaveOn(String leaveOn) {
        LeaveOn = leaveOn;
    }
}
