package com.example.Outpatient.Appointment.Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OutPatientAppointmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(OutPatientAppointmentApplication.class, args);
	}

}
