package com.example.Outpatient.Appointment.Application.Controller;

import com.example.Outpatient.Appointment.Application.Model.Doctor_Listing;
import com.example.Outpatient.Appointment.Application.Repository.Doctor_ListingRepository;
import com.example.Outpatient.Appointment.Application.Service.Doctor_Listing_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Repository
@RequestMapping("/Doctor_Listing")
public class Doctor_ListingController {
    @Autowired
    private Doctor_Listing_Service doctorListingService;
    @Autowired
    private Doctor_ListingRepository doctor_ListingRepository;

    @PostMapping("/interject")
    public Doctor_Listing addDoctor_Listing(@RequestBody Doctor_Listing doctorListing){
        Doctor_Listing obj = doctorListingService.addDoctor_Listing(doctorListing);
        return obj;
    }
    @GetMapping("/list")
    public ResponseEntity<List<Doctor_Listing>> listDoctors(){
        List<Doctor_Listing> doctorListings = doctor_ListingRepository.findAll();
        return ResponseEntity.ok(doctorListings);
    }



}
