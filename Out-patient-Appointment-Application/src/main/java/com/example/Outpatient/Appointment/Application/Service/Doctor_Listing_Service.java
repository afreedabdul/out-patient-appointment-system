package com.example.Outpatient.Appointment.Application.Service;

import com.example.Outpatient.Appointment.Application.Controller.Doctor_ListingController;
import com.example.Outpatient.Appointment.Application.Model.Doctor_Listing;
import com.example.Outpatient.Appointment.Application.Repository.Doctor_ListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Doctor_Listing_Service {
    @Autowired
    private Doctor_ListingRepository doctorListingRepository;
    public Doctor_Listing addDoctor_Listing(Doctor_Listing doctorListing) {
        doctorListingRepository.save(doctorListing);
        return doctorListing;
    }
}
