package com.example.Outpatient.Appointment.Application.Service;

import com.example.Outpatient.Appointment.Application.Model.Appointment_Booking;
import com.example.Outpatient.Appointment.Application.Repository.Appointment_BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Appointment_Booking_Service {
    @Autowired
    private Appointment_BookingRepository appointmentBookingRepository;
    public Appointment_Booking addAppointment_Booking(Appointment_Booking appointmentBooking) {
        //try {
            appointmentBookingRepository.save(appointmentBooking);
            return appointmentBooking;
//        }catch(Exceptionp e){
//            System.out.print("error in storing");
//            return  appointmentBooking;
//        }
    }
}
